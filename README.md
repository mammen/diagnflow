
<!-- README.md is generated from README.Rmd. Please edit that file -->

# diagnFlow

<!-- badges: start -->
<!-- badges: end -->

The goal of diagnFlow is to provide Preprocessing, Modelling and Cross-Validation methods for clinical multi-tube Flow Cytometry data.

## Installation

You can install the released version of diagnFlow from this git repository with:

``` r
install.packages("remotes")
remotes::install_git("https://git.embl.de/mammen/diagnflow.git")
```

<!-- ## Example -->
<!-- ToDo: Basic example which shows the approximate workflow: -->
<!-- ```{r example} -->
<!-- library(diagnFlow) -->
<!-- ## basic example code -->
<!-- ``` -->
