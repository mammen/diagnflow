
X <- matrix(c(1:50, 100:51) + 0.1*stats::rnorm(100), nrow = 50)
colnames(X) <- c("var1", "var2")
y <- rep(0, 25) %>%
  c(rep(1, 25))
empty_diagnfcs <- new_diagnFCS(data = NULL,
                               metadata = tibble::tibble(
                                 Condition = rep("Class 1", 25) %>%
                                   c(rep("Class 2", 25))),
                               PositiveCondition = "Class 1")


test_that("model_glmnet() works", {
  # 1: The function works as intended with a numeric class
  set.seed(2463)
  mod1 <- model_glmnet(X, y)
  expect_equal(mod1$model(mod1, X), as.character(y))
  # 2: The function works as intended with a factor response variable
  set.seed(2463)
  mod2 <- model_glmnet(X, empty_diagnfcs$metadata$Condition)
  expect_equal(mod2$model(mod2, X),
               as.character(empty_diagnfcs$metadata$Condition))
})

test_that("model_caret() works", {
  # 1: The function works as intended with a numeric class
  set.seed(2463)
  mod1 <- model_caret(X, y, method = "rf")
  expect_equal(mod1$model(mod1, X), as.character(y))
  # 2: The function works as intended with a factor response variable
  set.seed(2463)
  mod2 <- suppressMessages(
    model_caret(X, empty_diagnfcs$metadata$Condition, method = "rf"))
  expect_equal(mod2$model(mod2, X),
               as.character(empty_diagnfcs$metadata$Condition))
})
