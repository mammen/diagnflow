
test_that("use_binning() provides exactly the expected output for one- and two-dimensional binning.", {
  expect_error(use_binning("Unvalid Argument", dims = 1))

  ff <- (system.file("extdata", "0877408774.B08", package = "flowCore") %>%
           flowCore::read.FCS())

  bin <- 2
  tmp <- do.call(expand.grid, rep(list(1:bin), ncol(ff)))
  ff <- ff[1:nrow(tmp),]
  flowCore::colnames(tmp) <- flowCore::colnames(ff)
  flowCore::exprs(ff) <- tmp %>%
    as.matrix()

  dim <- c(1,2)
  expect_equal({
    use_binning(ff, dims = dim, bins = bin) %>%
      as.numeric()},
    sapply(dim, function(dim){
      rep(bin^(ncol(ff)-dim),
          (bin^dim)*choose(ncol(ff), dim)) %>%
        tibble::as_tibble_row(
          .name_repair = ~ vctrs::vec_as_names(
            ..., repair = "minimal", quiet = TRUE))
    }) %>%
      dplyr::bind_cols(
        .name_repair = ~ vctrs::vec_as_names(
          ..., repair = "minimal", quiet = TRUE)) %>%
      as.numeric())
})
