bin <- 2
dim <- 1

fcs <- readRDS("diagnFCS_testdata.RDS") %>%
  apply_QC2ff(function(ff, verbose = FALSE){
    tmp <- do.call(expand.grid, rep(list(1:bin), ncol(ff)))
    if(verbose) message(tmp)
    colnames(tmp) <- colnames(ff)
    exprs(ff) <- tmp %>%
      as.matrix()
    if(verbose) message(colnames(ff))
    return(ff)
  }, slot_in = "data")
fcs <- create_lookup_matrix(fcs)

test_that(
  "generate_mat produces the expected matrix",
  {
    expect_equal({
      fcs <- generate_mat(
        fcs = fcs,
        directory = tempdir(),
        id = "test", generate_mat_methods = "binning",
        binning_dims = dim, binning_bins = bin, verbose = TRUE,
        save = FALSE)
      fcs$mod$train$raw %>%
        as.matrix() %>%
        as.numeric()
    },
    matrix(2048, nrow = nrow(fcs$metadata),
           ncol = (sum(!is.na(fcs$lookup_matrix)) +
             3*sum(apply(!is.na(fcs$lookup_matrix), 1, any))) *
             bin) %>%
      as.numeric())
  })
