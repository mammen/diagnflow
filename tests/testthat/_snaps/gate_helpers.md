# lookup_matrix extracts the correct row names for our test dataset

    structure(list(FITC.A = c(NA, "Lambda", "CD8", NA, "FMC7", "CD103", 
    "IgM", "CD24"), PE.A = c(NA, "Kappa", "CD56", NA, "CD23", "CD11c", 
    "CD200", "CD79b"), PerCP.Cy5.5.A = c(NA, "CD20", "CD4", NA, "CD20", 
    "CD20", "CD20", "CD20"), PE.Cy7.A = c(NA, "CD19", "CD19", NA, 
    "CD19", "CD19", "CD19", "CD19"), APC.A = c(NA, "CD5", "CD3", 
    NA, "CD22", "CD25", "CD38", "CD43"), APC.Cy7.A = c(NA, "CD45 APC-H7", 
    NA, NA, NA, NA, NA, NA)), class = c("tbl_df", "tbl", "data.frame"
    ), row.names = c(NA, -8L))

