#' Load diagnFCS slots as saved in folder structure
#'
#' @param fcs diagnFCS object with slots data and metadata
#' @param directory folder containing clean.RDS and further subfolders
#' @param verbose Verbosity
#'
#' @return S3 Object of Class diagnFCS
#' @export
load_slots <- function(fcs, directory, verbose = FALSE) {
  if(verbose) message("Loading from ", directory)
  files <- list.files(directory, full.names = FALSE, recursive = TRUE) %>%
    stringr::str_subset("pdf$", negate = TRUE)
  for(file in files){
    struc <- strsplit(file, "\\.")[[1]][[1]] %>% strsplit("/") %>% unlist()
    if(verbose) message(paste(struc, collapse = " > "))
    for(i in 1:length(struc)){
      if(is.null(`[[`(fcs, struc[1:i]))) `[[`(fcs, struc[1:i]) <- list()
    }
    `[[`(fcs, struc) <- readRDS(file.path(directory, file))
  }
  return(fcs)
}

#' Create diagnFCS object for downstream use Low level constructor
#'
#' @param data  List of uniform flowSets constituting different sample aliquots
#'   ("tubes")
#' @param metadata A [tibble::tibble()], metadata with one row per data sample
#' @param PositiveCondition Mostly semantics. The class to be predicted in
#'   binomial classification
#'
#' @details Using a S3 object as more of a quick-and-dirty solution
#' @return S3 Object of Class diagnFCS
#'
new_diagnFCS <- function(data, metadata, PositiveCondition = NULL) {
  if(!is.null(PositiveCondition)){
    stopifnot("Condition" %in% colnames(metadata))
    metadata <- metadata %>%
      dplyr::mutate(
        Condition = factor(.data$Condition,
                           levels = c(
                             unique(.data$Condition[!.data$Condition ==
                                                      PositiveCondition]),
                             PositiveCondition
                           )),
        individual_no = seq_along(Condition)
      )
  }
  structure(list(
    "data" = data,
    "metadata" = metadata
  ),
  class = "diagnFCS"
  )
}

#' Exposed function to create diagnFCS objects
#'
#' Calls upon low-level constructor `new_diagnFCS()`, `create_lookup_matrix()`
#' and `validate_diagnFCS()`
#'
#' @param data  List of uniform flowSets constituting different sample aliquots
#'   ("tubes")
#' @param metadata A [tibble::tibble()], metadata with one row per data sample
#' @param PositiveCondition Mostly semantics. The class to be predicted in
#'   binomial classification
#'
#' @return S3 Object of Class diagnFCS
#' @export
#'
diagnFCS <- function(data, metadata, PositiveCondition = NULL) {
  if (!tibble::is_tibble(metadata)) metadata <- tibble::as_tibble(metadata)
  new_diagnFCS(data, metadata, PositiveCondition) %>%
    # Fails if markers of respective not homogenous
    create_lookup_matrix() %>%
    validate_diagnFCS()
}

#' Validate diagnFCS objects
#'
#' @param objec  List of uniform flowSets constituting different sample aliquots
#'   ("tubes")
#'
#' @return S3 Object of Class diagnFlow
#'
validate_diagnFCS <- function(objec) {
  stopifnot(is.list(objec$data))
  stopifnot(tibble::is_tibble(objec$metadata))

  if (!all(class(objec) == "diagnFCS")) stop("Object not of class diagnFCS")
  if (!is.list(objec)) stop("Object is not a list")
  if (!"data" %in% names(objec)) stop('No slot called "data"')
  if (!"metadata" %in% names(objec)) stop('No slot called "metadata"')
  if (!all(sapply(objec$data, function(clas) class(clas) == "flowSet"))) {
    stop("Non-flowSet elements in data slot")
  }
  if (nrow(objec$metadata) !=
      length(objec$data[[1]])) stop('Row number of metadata != sample number')
  return(objec)
}

#' @method print diagnFCS
#' @export
print.diagnFCS <- function(x, ...) {
  # Bug in roxygen2 makes it not recognise this as a method, which makes the
  # explicit @method necessary
  attri <- attributes(x)
  message(
    "diagnFCS object\n",
    ifelse(length(attri$names) == 1, "Slot: ", "Slots: "),
    '"', paste0(attri$names, collapse = '", "'), '"'
  )
  invisible(x)
}
