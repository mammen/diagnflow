#' Wrapper for modelling functions
#'
#' Wraps function like [glmnet::cv.glmnet()] to provide generic access in a
#' hyperparameter tuning setup
#'
#' @param X Matrix
#' @param y Factor with 2 levels
#' @param ... Passed to wrapped function
#' @param verbose logical(1), Should the function output progress messages
#'
#' @return \describe{A named list:
#' \item{model}{predictive model that predicts y' from the output of the wrapped function and X'}
#' \item{object}{output of the wrapped function, e.g. object of class [cv.glmnet]}}
#'
#' @name model_
NULL
#> NULL
#' @describeIn model_ A wrapper for [glmnet::cv.glmnet()]
#'
#' @param family Passed to [glmnet::cv.glmnet()]
#' @param alpha Passed to [glmnet::cv.glmnet()]
#' @param standardize Passed to [glmnet::cv.glmnet()]
#' @param type.measure Passed to [glmnet::cv.glmnet()]
#' @param nfolds Passed to [glmnet::cv.glmnet()]
model_glmnet <- function(X, y,
                         family = "binomial", alpha = 1,
                         standardize = TRUE,
                         type.measure = "class", nfolds = 10,
                         ..., verbose = FALSE){
  X <- as.matrix(X)
  y <- as.factor(y)
  if(length(levels(y)) != 2){stop("Multinomial Classification coming soon.")}
  y_num <- factor(y, labels = 0:(length(levels(y)) - 1))
  mod <- glmnet::cv.glmnet(X, y_num,
                           family = family, alpha = alpha,
                           standardize = standardize,
                           type.measure = type.measure, nfolds = nfolds,
                           ...)
  cutoff <- stats::predict(mod,
                    newx = X, s = "lambda.min",
                    type = "response") %>%
    as.numeric() %>%
    optimize_cutoff(y_num)
  return(list(model =
                function(model_output, newX, s = "lambda.min",
                         verbose = FALSE){
                  prob <- stats::predict(model_output$object,
                                  newx = as.matrix(newX), s = s,
                                  type = "response") %>%
                    as.numeric()
                  ifelse(prob >= model_output$cutoff,
                         model_output$lvls[[2]],
                         model_output$lvls[[1]]) %>%
                    return()
                },
              object = mod,
              cutoff = cutoff,
              lvls = levels(y)
  ))
}

#' @param method Passed to [caret::train()]
#' @param ... Passed to [caret::train()]
#'
#' @describeIn model_ A wrapper for [caret::train()]
model_caret <- function(X, y, method, ..., verbose = FALSE){
  X <- as.matrix(X)
  y <- as.factor(y)
  if(length(levels(y)) != 2){stop("Multinomial Classification coming soon.")}
  # y_num <- factor(y, labels = 0:(length(levels(y)) - 1))
  mod <- caret::train(X, y, method = method, ...)
  return(list(model = function(model_output, newX, verbose = FALSE){
    stats::predict(model_output$object, newX) %>%
      as.character()
  },
  object = mod))
}

#'Find the optimal cutoff for a model with a continous output using an
#'equivalent of the Youden Index
#'
#'To be implemented, e.g. for model_glmnet, controlled by a parameter to
#'model_glmnet of the form optimize_cutoff = TRUE
#'
#'@param mod_output Output of model for training data
#'@param y Training Ground Truth
#'
#'@return A numeric vector of length 1 between 0 and 1
optimize_cutoff <- function(mod_output, y){
  roc <- ROCR::performance(
    ROCR::prediction(mod_output, y),
    "sens", x.measure = "spec")

  cutoff <- roc@alpha.values[[1]][
    which.max(roc@x.values[[1]] +
                roc@y.values[[1]])]
  return(cutoff)
}
